import os 
import sys
import csv  
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
def getUserInfo(users,access):
    user = "null"
    x=0
    while x<len(users):
        if users[x][1]==access:
            user = users[x]
            break
        x = x+1
    return user

def searchUsers(users, srch):
    while srch != "done":
        found = 0
        x=0
        while x<len(users):
            if(srch.lower() in users[x][0].lower())or(srch.lower() in users[x][1].lower())or(srch.lower() in users[x][2].lower())or(srch.lower() in users[x][3].lower())or(srch.lower() in users[x][4].lower())or(srch.lower() in users[x][5].lower()):
                found = 1
                print users[x]
            x = x+1
        if found==0:
            print"No users found"
        srch = raw_input("\nEnter search term: ")
        
def getUserList(fileName):
    users = []
    # itterate through file and split
    with open(fileName,'rb')as f:
        for row in csv.reader(f,delimiter=',',skipinitialspace=True):
            users.append(row)
    x =0
    email_row = -1
    access_row = -1
    first_row = -1
    last_row = -1
    sid_row = -1
    fav_row = -1
    while (x < len(users[0])):
        curr = users[0][x]
        if (curr == "Email Address"):
            email_row = x
        elif (curr == "Access Number"):
            access_row=x
        elif(curr=="First Name"):
            first_row=x
        elif(curr=="Last Name"):
            last_row=x
        elif(curr=="SID"):
            sid_row =x
        elif(curr=="Favourite Film"):
            fav_row=x 
        x = x+1
    newUsers = []
    x = 1 
    while x <len(users)-2:
        currUser = users[x]
        newUsers.append([currUser[email_row],currUser[access_row],currUser[first_row],currUser[last_row],currUser[sid_row],currUser[fav_row]])
        x = x+1
    return newUsers

def getTerminalSize():
    env = os.environ
    def ioctl_GWINSZ(fd):
        try:
            import fcntl, termios, struct, os
            cr = struct.unpack('hh', fcntl.ioctl(fd, termios.TIOCGWINSZ,
        '1234'))
        except:
            return
        return cr
    cr = ioctl_GWINSZ(0) or ioctl_GWINSZ(1) or ioctl_GWINSZ(2)
    if not cr:
        try:
            fd = os.open(os.ctermid(), os.O_RDONLY)
            cr = ioctl_GWINSZ(fd)
            os.close(fd)
        except:
            pass
    if not cr:
        cr = (env.get('LINES', 25), env.get('COLUMNS', 80))

        ### Use get(key[, default]) instead of a try/catch
        #try:
        #    cr = (env['LINES'], env['COLUMNS'])
        #except:
        #    cr = (25, 80)
    return int(cr[1]), int(cr[0])

def clearScreen():
    #clear screen (check for operating system)  
    if sys.platform == "darwin":
        os.system('clear')

def printOposite(item1,item2):
    print item1+" "*(getTerminalSize()[0]-len(item1)-len(item2))+item2

def removeFromTill(till,amount,reason,tillFile):
    tillFile.write(str(till-amount)+"\t-"+str(amount)+"\t"+reason+"\n")
    return till - amount

def addToTill(till,amount,reason,tillFile):
    tillFile.write(str(till+amount)+"\t+"+str(amount)+"\t"+reason+"\n") 
    return  till + amount       

def runEvent():
    eventName = raw_input('Enter event name: ')
    accessPrice = int(raw_input("Enter access price: "))
    nonAccessPrice = int(raw_input("Enter non-access price: "))
    tillFloat = int(raw_input("Enter till float: "))
    count =0
        
    #open files
    os.mkdir(eventName)
    attendeesFile = open(eventName+"/"+eventName+" - attendees.txt",'a')
    newUsersFile = open(eventName+"/"+eventName+" - new users.csv",'a')
    tillFile = open(eventName+"/"+eventName+" - till.txt",'a')

    #update files with event information
    tillFile.write("TILL FILE\n---\n")
    tillFile.write("Event: "+eventName+"\n")    
    tillFile.write("Access price: $"+str(accessPrice)+" ")
    tillFile.write("Non-Access price: $"+str(nonAccessPrice)+"\n")
    tillFile.write("----\n")
    tillFile.write("($)\t+/-\tDesc\n")
    tillFile.write(str(tillFloat)+"\t+"+str(tillFloat)+"\tFloat\n")
    

    clearScreen()
    accessNo = "0"
    user_name = "null"
    user_email = "null"
    user_sid = "null"
    user_fav = "null"
    while accessNo != "done":
        #Print event Info
        clearScreen()
        countLabel = "count: "+str(count)
        printOposite(eventName,countLabel)
        printOposite("Access: $"+str(accessPrice),"Till: $"+str(tillFloat))
        print "Non-Access: $"+str(nonAccessPrice)
        print "-"*getTerminalSize()[0]
        #If access number was entered
        if (accessNo=="non-access")or((len(accessNo)==6)and(user_name != "null")):
            print "##ADDED TO EVENT##"
            print "Name: "+user_name
            print "ACCESS: "+accessNo
            print "\nEmail: "+user_email
            print "SID: "+user_sid
            print "Favourite Film/s: "+user_fav
            
            #add user to atendees file
            attendeesFile.write(accessNo + " "+user_name+"\n")
            count = count +1
            if len(accessNo)==6:
                tillFloat = addToTill(tillFloat,accessPrice,"Access: "+accessNo,tillFile)
                #tillFloat = tillFloat + accessPrice 
            elif accessNo=="non-access":
                #tillFloat = tillFloat + nonAccessPrice
                tillFloat = addToTill(tillFloat,nonAccessPrice,"Non-Access: "+user_name,tillFile) 

            #Reset user values to default
            user_fav = "null"
            user_sid = "null"
            user_email = "null"
            user_name = "null"
    
        #Run for Help command
        elif (accessNo =="help"):
            print "##HELP##"
            print "You are currently in event mode. This allows you to add users and make users to add to the currently running event.\n"
            print "COMMAND LIST"
            print ""
            print " - t+ or +t: Add money to the till"
            print " - t- or -t: Remove money from the till"
            print ""
            print " - na or non-access: Add a non-access member to the event" 
            print ""
            print " - done: End's event and return to main menu"
        
        elif(accessNo=="srch"):
            print "##SEARCHING MEMBERS##"
            srch = raw_input("Enter search term: ")
            searchUsers(users,srch)         
        #run if incoroect code is entered
        #elif (accessNo !="0")and(len(accessNo)!=6):
            #print "Incorrect format, please try again"

        #GET INPUT
        accessNo = raw_input("\nPlease enter access number/command: ")
        #If access number (ie. 6 digits)
        if len(accessNo)==6: 
            #If access number is intered get info
            user = getUserInfo(users,accessNo)
            if user != "null":
                user_name = user[2] + " " +user[3]
                user_email = user[0]
                user_sid = user [4]
                user_fav = user[5]
            #if user is not in database give options to add to mailing list
            else:
                result = raw_input("User doesn't exsist, would you like to make new member? y/n/c: ")
                if result=="y":
                    #make new user
                    fName = raw_input("Please enter first name: ")
                    lName = raw_input("Please enter last name: ")
                    user_name = fName +" "+lName
                    user_email = raw_input("please enter email: ")
                    newUsersFile.write(accessNo+","+fName+","+lName+","+user_email+"\n")    
                elif result=="n":
                    #insert user regardless
                    fName = raw_input("Please enter first name: ")
                    lName = raw_input("Please enter last name: ")
                    user_name = fName +" "+lName
        elif (accessNo=="+t") or (accessNo=="t+"):
            print "##ADD TO TILL##"
            amount = raw_input("Amount: ")
            reason = raw_input("Reason: ")
            tillFloat = addToTill(tillFloat,int(amount),reason,tillFile)
            #tillFloat = tillFloat + int(amount)
        elif(accessNo =="-t") or (accessNo=="t-"):
            print "##REMOVE FROM TILL##"    
            amount = raw_input("Amount: ")
            reason = raw_input("Reason: ")
            tillFloat = removeFromTill(tillFloat,int(amount),reason,tillFile)
            #tillFloat = tillFloat - int(amount)
        elif(accessNo == "na") or (accessNo=="non-access"):
            print "##ADDING NON ACCESS MEMBER##"
            accessNo = "non-access"
            fName = raw_input("Please enter first name: ")
            lName = raw_input("Please enter last name: ")
            user_name = fName +" "+lName
            newUsersFile.write(accessNo+","+fName+","+lName+"\n")   


userInput = ""
while (userInput!="quit") and (userInput!="done"):
    clearScreen()
    users = getUserList("users.csv")
    print "you have a couple choices:"
    print "1. Start event"

    userInput = raw_input("\nplease enter a number: ")
    if userInput == "1":
        runEvent()
print "\nWe're quitting here..."
    
